# capture


a nats service to create pcap files


# services

## service general status

    get.capture.status  
    
        out: { 
            freeMemory: uint64 
            pcalist [] string 
            
         }
    
## clear all pcap files 
    
delete all pcap files stored 

    call capture.pcap.clear
            
# capturing pcap files


## capture  a pcap file  ( => /tmp/<name>)

    call.capture.pcap.start
       in:  { 
            Name: string          // name of file eg  capture_001
            Interface string  
            Filter string 
            Count uint64         // optional
            Duration : uint64    // in seconds , optional 
        }
        
        out:  { 
            Rid: string     //   eg capture.pcap.capture_001
         
        }
        
## get a capture status
    
    get.capture.pcap.$id  
        out: {
            running bool
            size int
        }
    
    
    
## stop  a pcap  capture 

     call capture.pcap.$id.stop 
     

# reading pcap files

## read a pcap file   capture.readpcap


    call.capture.readpcap.open  
        in { 
            name string
            filter string   // a BPF filter  eg udp and tcp 
        }
        
    call.capture.readpcap.$id.next   // read next packet 
        out: {
            data []byte
        }
    
    call.capture.readpcap.$id.close
    
    
